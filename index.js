/**Task 2:
1. Read the data from the file
2. Calculate the top 10 mutual funds based on year_5 returns. Use higher order functions
3. Write the result to a new file */
//Calculate the average year_1 return rate per fund house

const fs = require("fs");

fs.readFile("./funds.json", "utf-8", (err, data) => {
	if (err) console.log(err);
	const resObj = JSON.parse(data);
	resObj.sort((a, b) => b.returns.year_5 - a.returns.year_5);
	const res = resObj.slice(0, 10);
	fs.writeFile("./abc.json", JSON.stringify(res), (err) => {
		if (err) console.log(err);
		else console.log("Data written Successfully...");
	});
	const yearSum = resObj.reduce((acc, element) => {
		const fundHouse = element.fund_house;
		const year = element.returns.year_1;

		if (year !== undefined) {
			if (acc[fundHouse] !== undefined) {
				acc[fundHouse] += parseFloat(year);
			} else {
				acc[fundHouse] = parseFloat(year);
			}
			//}
		}
		return acc;
	}, {});
	//console.log(yearSum);
	const yearCount = resObj.reduce((acc, element) => {
		const fundHouse = element.fund_house;
		const year = element.returns.year_1;

		if (year !== undefined) {
			if (acc[fundHouse] !== undefined) {
				acc[fundHouse] += 1;
			} else {
				acc[fundHouse] = 1;
			}
		}
		return acc;
	}, {});
	//console.log(yearCount);
	const ans = {};
	for (let fund in yearSum) {
		let val1 = yearSum[fund];
		let val2 = yearCount[fund];
		let value = val1 / val2;
		ans[fund] = value.toFixed(2);
	}
	console.log(ans);
});
